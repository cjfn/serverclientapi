﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiCrud.Models
{
    public class LoginRequest
    {
        public string nombre { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public int tipo_usuario { get; set; }
        public DateTime FechaVencimientoContraseña { get; set; }
        public string Mensaje { get; set; }
        public string Token { get; set; }
    }
}