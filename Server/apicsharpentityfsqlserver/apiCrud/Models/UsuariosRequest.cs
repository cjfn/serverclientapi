﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiCrud.Models
{
    public partial class UsuariosRequest
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public int tipo_usuario { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        
    }
}