﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace apiCrud.Models
{
    public class MessageRequest
    {
        public int id { get; set; }
        public string msg { get; set; }
        public string tags { get; set; }
    }
}