﻿using apiCrud.DataEntity;
using apiCrud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace apiCrud.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private TESTEntities db = new TESTEntities();

        [HttpPut]
        [Route("credential")]
        public IHttpActionResult credential(LoginRequest login)
        {
            if (login == null)
            {
                return Unauthorized();
            }
            else
            {
                Usuarios data = db.Usuarios.Where(x => x.email == login.email).FirstOrDefault<Usuarios>();

                if (data != null)
                {
                    var token = TokenGenerator.GenerateTokenJwt(login.pass);
                    return Ok(token);
                }
                else
                {
                    return Unauthorized();
                }
            }

        }

    }
}
