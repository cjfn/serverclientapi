﻿using apiCrud.DataEntity;
using apiCrud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;


namespace apiCrud.Controllers
{
    [Authorize]
    [RoutePrefix("api/MesageData")]
    public class MesageDataController : ApiController
    {
      
        private TESTEntities db = new TESTEntities();
        // GET: Usuarios

        [HttpPost]
        [Route("message")]
        public IHttpActionResult message(MessageRequest message)
        {
            if(message.msg == String.Empty && message.tags == String.Empty)
            {
                return BadRequest("Datos requeridos");
            }
            else
            {

                try
                {
                    var insert = db.Message_Data.Add(new Message_Data()
                    {
                        id = message.id, 
                        msg = message.msg,
                        tags = message.tags,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();
                    var id = insert.id;
                    return Ok( id);
                }
                catch (Exception ex)
                {
                    return BadRequest("Error en Creacion: " + ex.Message);
                }
            }
        }
        [HttpGet]
        [Route("messages/{req}")]

        public IHttpActionResult messages(string req)
        {
            if(req == String.Empty)
            {
                // without results
                return BadRequest("Search without results");
            }
            else
            {
                try
                {
                    var id = Convert.ToInt32(req);
                    //message by id
                    Message_Data msg = db.Message_Data.Where(x => x.id == id).FirstOrDefault();
                    return Ok(msg);

                }
                catch (Exception ex)
                {
                    //all messages
                    IList<Message_Data> data = null;
                    data = db.Message_Data.ToList<Message_Data>();
                    return Ok(data);
                }
            }

        }

    }
}
