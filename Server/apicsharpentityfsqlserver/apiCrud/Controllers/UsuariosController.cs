﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using apiCrud.DataEntity;
using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;
using HttpPostAttribute = System.Web.Mvc.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;

namespace apiCrud.Controllers
{
    [Authorize]
    [RoutePrefix("Usuarios")]
    public class UsuariosController : ApiController
    {
        private TESTEntities db = new TESTEntities();
        // GET: Usuarios

        [HttpPost]
        [Route("message")]
        public IHttpActionResult message()
        {
            IList<Usuarios> data = null;
            data = db.Usuarios.ToList<Usuarios>();
            return Ok(data);
        }



        // GET: usuario/Details/5
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.Route("api/usuarios/{id}")]
        public IHttpActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest("Dato requerido");
            }
            Usuarios data = db.Usuarios.Find(id);
            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Mvc.Route("api/usuarios/")]
        public IHttpActionResult Create(Usuarios usuario)
        {
            try
            {
                db.Usuarios.Add(new Usuarios()
                {
                    id = usuario.id,
                    nombre = usuario.nombre,
                    email = usuario.email,
                    pass = usuario.pass,
                    tipo_usuario = usuario.tipo_usuario,
                    created_at = DateTime.Now,
                    updated_at = null
                });
                db.SaveChanges();
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Creacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Mvc.Route("api/usuarios/{id}")]
        public IHttpActionResult Edit(int? id, Usuarios usuario)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                usuario.updated_at = DateTime.Now;
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return Ok("Dato modificado exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en modificacion: " + ex.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Mvc.Route("api/usuarios/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                Usuarios usuario = db.Usuarios.Find(id);
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                db.Entry(usuario).State = EntityState.Deleted;
                db.SaveChanges();
                return Ok("Datos eliminados exitosamente");
            }
            catch (Exception ex)
            {
                return BadRequest("Error en Eliminación: " + ex.Message);
            }

        }
    }
}