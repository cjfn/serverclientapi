//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace apiCrud.DataEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usuarios
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public int tipo_usuario { get; set; }
        public System.DateTime created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
    
        public virtual Tipo_usuario Tipo_usuario1 { get; set; }
    }
}
